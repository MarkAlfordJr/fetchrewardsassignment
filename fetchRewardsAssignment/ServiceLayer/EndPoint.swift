//
//  EndPoint.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import Foundation

protocol EndPoint {
    // HTTP or HTTPS
    var scheme: String {get}
    // url
    var baseUrl: String {get}
    // paths after baseUrl
    var path: String {get}
    // any Parameters for API call
    var parameters: [URLQueryItem] {get}
    // GET,POST, etc
    var method: String {get}
}
