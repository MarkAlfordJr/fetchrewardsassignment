//
//  MealsModel.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import Foundation

// MARK: - Welcome
struct MealsModel: Codable {
    let meals: [Meal]
}

// MARK: - Meal
struct Meal: Codable {
    let strMeal: String
    let strMealThumb: String
    let idMeal: String
}
