//
//  IdMealsModel.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import Foundation

struct IdMealsModel: Codable {
    let meals: [[String: String?]]
}
