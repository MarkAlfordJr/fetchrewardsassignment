//
//  CategoriesModels.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import Foundation

// MARK: - Welcome
struct CategoriesModel: Codable {
    let categories: [Category]
}

// MARK: - Category
struct Category: Codable {
    let idCategory, strCategory: String
    let strCategoryThumb: String
    let strCategoryDescription: String
}
