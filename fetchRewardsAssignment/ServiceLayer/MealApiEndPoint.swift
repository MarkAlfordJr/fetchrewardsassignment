//
//  MealApiEndPoint.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import Foundation

enum MealApiEndPoint: EndPoint {
    case fetchCategories(fetch: String = "categories")
    case getMealByCategory(searchCategory: String)
    case getMealById(searchID: String)
    
    var scheme: String {
        switch self {
        default:
            return "https"
        }
    }
    
    var baseUrl: String {
        switch self {
        default:
            return "www.themealdb.com"
        }
    }
    
    var path: String {
        switch self {
        case .fetchCategories:
            return "/api/json/v1/1/categories.php"
        case .getMealByCategory:
            return "/api/json/v1/1/filter.php"
        case .getMealById:
            return "/api/json/v1/1/lookup.php"
        }
    }
    
    var parameters: [URLQueryItem] {
        switch self {
        case .fetchCategories(fetch: _):
            return [
            ]
        case .getMealByCategory(searchCategory: let searchCategory):
            return [
                URLQueryItem(name: "c", value: searchCategory)
            ]
        case .getMealById(searchID: let searchID):
            return [
                URLQueryItem(name: "i", value: searchID)
            ]
        }
        
    }
    
    var method: String {
        return "GET"
    }
    
    
}
