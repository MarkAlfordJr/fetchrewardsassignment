//
//  ApiManager.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import Foundation

class ApiManager: Operation {
    
    class final func fetchApi<T: Codable>(endpoint: EndPoint, completion: @escaping (Result<T, Error>) -> ()) {
        
        var components = URLComponents()
        components.scheme = endpoint.scheme
        components.host = endpoint.baseUrl
        components.path = endpoint.path
        components.queryItems = endpoint.parameters
        
        guard let url = components.url else {return}
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = endpoint.method
        
        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else {
                completion(.failure(error!))
                print(error?.localizedDescription ?? "Unknown Error")
                return
            }
            
            guard response != nil, let safeData = data else { return }
            
            DispatchQueue.main.async {
                if let responseObject = try? JSONDecoder().decode(T.self, from: safeData) {
                    completion(.success(responseObject))
                } else {
                    let error = NSError(domain:"", code: 200, userInfo: [NSLocalizedDescriptionKey: "error"])
                    completion(.failure(error))
                }
            }
        }
        dataTask.resume()
    }
}
