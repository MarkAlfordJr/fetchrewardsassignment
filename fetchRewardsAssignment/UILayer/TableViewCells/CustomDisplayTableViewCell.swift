//
//  CustomDisplayTableViewCell.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import UIKit

class CustomDisplayTableViewCell: UITableViewCell {
    
    static let identifier = "CustomCell"
    
    let foodImage: UIImageView = {
        let image = UIImageView()
        // will get imagery from url data
        image.backgroundColor = .cyan
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let titleText: UILabel = {
        let textField = UILabel()
        textField.textColor = .black
        textField.backgroundColor = .clear
        textField.numberOfLines = 0
        textField.lineBreakMode = .byWordWrapping
        return textField
    }()

    let descriptionText: UILabel = {
        let textField = UILabel()
        textField.textColor = .gray
        textField.backgroundColor = .clear
        textField.numberOfLines = 0
        textField.lineBreakMode = .byWordWrapping
        return textField
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addCellSubViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addCellSubViews() {
        contentView.addSubview(foodImage)
        contentView.addSubview(titleText)
        contentView.addSubview(descriptionText)
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        foodImage.translatesAutoresizingMaskIntoConstraints = false
        titleText.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.translatesAutoresizingMaskIntoConstraints = false
        
        //MARK: - Image
        foodImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        foodImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        foodImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
        foodImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
        
        //MARK: - Title
        titleText.centerYAnchor.constraint(equalTo: foodImage.centerYAnchor).isActive = true
        titleText.leadingAnchor.constraint(equalTo: foodImage.trailingAnchor, constant: 8).isActive = true
        titleText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        
        //MARK: - Description
        descriptionText.topAnchor.constraint(equalTo: foodImage.bottomAnchor).isActive = true
        descriptionText.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
        descriptionText.leadingAnchor.constraint(equalTo: foodImage.trailingAnchor, constant: 8).isActive = true
        descriptionText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 8).isActive = true
    }
    
    // values come from API response
    func configureCell(image foodImage: String, title: String, description: String? = nil) {
        self.foodImage.loadImageFromURL(urlString: foodImage, placeholder: UIImage(systemName: "house"))
        self.titleText.text = title
        self.descriptionText.text = description
    }

}
