//
//  MealInstructionsTableViewCell.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/18/22.
//

import UIKit

class MealInstructionsTableViewCell: UITableViewCell {

    static let identifier = "InstructionsCustomCell"
    
    let titleText: UILabel = {
        let textField = UILabel()
        textField.textColor = .green
        textField.backgroundColor = .clear
        textField.lineBreakMode = .byWordWrapping
        return textField
    }()

    let descriptionText: UILabel = {
        let textField = UILabel()
        textField.textColor = .black
        textField.backgroundColor = .clear
        textField.textAlignment = .left
        textField.numberOfLines = 0
        textField.lineBreakMode = .byWordWrapping
        return textField
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addCellSubViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addCellSubViews() {
        contentView.addSubview(titleText)
        contentView.addSubview(descriptionText)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleText.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.translatesAutoresizingMaskIntoConstraints = false
        
        titleText.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12).isActive = true
        titleText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
        titleText.heightAnchor.constraint(equalToConstant: 20).isActive = true
        titleText.widthAnchor.constraint(equalToConstant: 250).isActive = true
        
        descriptionText.topAnchor.constraint(equalTo: titleText.bottomAnchor, constant: 8).isActive = true
        descriptionText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
        descriptionText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        descriptionText.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
    }

    func configureCell(title: String, description: String) {
        self.titleText.text = title
        self.descriptionText.text = description
    }
    
}
