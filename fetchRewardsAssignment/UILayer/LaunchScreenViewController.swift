//
//  ViewController.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import UIKit


class LaunchScreenViewController: UIViewController {

    var mealList = [Meal]()
    
    let searchTextField: UILabel = {
        let textField = UILabel()
        textField.backgroundColor = .white
        textField.text = "Explore some food!"
        textField.textAlignment = .center
        return textField
    }()
    
    let browseButton: UIButton = {
        let button = UIButton()
        button.setTitle("Browse", for: .normal)
        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(navToMainHub), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Launch"
        view.backgroundColor = .systemBackground
        addSubViews()
    }
    
    func addSubViews() {
        view.addSubview(searchTextField)
        view.addSubview(browseButton)
    }
    
    @objc func navToMainHub() {
        self.navigationController?.pushViewController(CategoryTableViewController(), animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        browseButton.translatesAutoresizingMaskIntoConstraints = false
        searchTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        searchTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        browseButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        browseButton.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 8).isActive = true
        browseButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
    }
}
