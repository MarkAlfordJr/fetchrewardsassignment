//
//  CategoryTableViewController.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import UIKit

class CategoryTableViewController: UITableViewController {
    

    var categoryList: [Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Categories"
        tableView.register(CustomDisplayTableViewCell.self, forCellReuseIdentifier: CustomDisplayTableViewCell.identifier)
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
        ApiManager.fetchApi(endpoint: MealApiEndPoint.fetchCategories(fetch: "category")) { (result: Result<CategoriesModel, Error>) in
            switch result {
            case .success(let result):
                // loop through ARRAY of objects
                DispatchQueue.main.async {
                    self.categoryList = result.categories
                    print(self.categoryList)
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let input = categoryList[indexPath.row].strCategory
        print(input)
        self.navigationController?.pushViewController(MealsTableViewController(apiInput: input), animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomDisplayTableViewCell.identifier, for: indexPath) as! CustomDisplayTableViewCell

        let images = categoryList[indexPath.row].strCategoryThumb
        let categoryTitle = categoryList[indexPath.row].strCategory
        cell.configureCell(image: images, title: categoryTitle)

        return cell
    }

}
