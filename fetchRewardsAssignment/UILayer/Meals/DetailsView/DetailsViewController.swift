//
//  DetailsViewController.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/18/22.
//

import UIKit

class DetailsViewController: UIViewController {

    var dictKeyArray = [String]()
    var dictValueArray = [String?]()
    var imageURL: String = ""
    var mealName: String = ""
    var mealInstruction: String = ""
    
    let tableView: UITableView = {
        let table = UITableView(frame: .null, style: .insetGrouped)
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.register(CustomDisplayTableViewCell.self, forCellReuseIdentifier: CustomDisplayTableViewCell.identifier)
        table.register(MealInstructionsTableViewCell.self, forCellReuseIdentifier: MealInstructionsTableViewCell.identifier)
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = view.bounds
        tableView.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
    }
    
    init(mealId: String, url: String, meal: String) {
        super.init(nibName: nil, bundle: nil)
        self.imageURL = url
        self.mealName = meal
        DispatchQueue.global(qos: .background).async {
            ApiManager.fetchApi(endpoint: MealApiEndPoint.getMealById(searchID: mealId)) {  (result: Result<IdMealsModel, Error>) in
                switch result {
                case .success(let result):
                    DispatchQueue.main.async {
                        let dict = result.meals
                        self.dictKeyArray = Array(dict[0].keys)
                        // returns oprional
                        self.dictValueArray = Array(dict[0].values)
                        self.tableView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return dictKeyArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CustomDisplayTableViewCell.identifier, for: indexPath) as! CustomDisplayTableViewCell
            cell.configureCell(image: imageURL, title: mealName)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: MealInstructionsTableViewCell.identifier, for: indexPath) as! MealInstructionsTableViewCell
        cell.configureCell(title: dictKeyArray[indexPath.row], description: dictValueArray[indexPath.row] ?? "")
        return cell
    }
}
