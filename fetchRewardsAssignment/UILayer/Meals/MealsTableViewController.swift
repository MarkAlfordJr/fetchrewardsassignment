//
//  MealsTableViewController.swift
//  fetchRewardsAssignment
//
//  Created by Mark Alford on 2/15/22.
//

import UIKit

class MealsTableViewController: UITableViewController {
    var mealList: [Meal] = []
    var parameterValue: String = ""
    var launchVC = LaunchScreenViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tableView.register(CustomDisplayTableViewCell.self, forCellReuseIdentifier: CustomDisplayTableViewCell.identifier)
    }
    
    init(apiInput: String) {
        super.init(nibName: nil, bundle: nil)
        title = apiInput
        DispatchQueue.global(qos: .background).async {
            ApiManager.fetchApi(endpoint: MealApiEndPoint.getMealByCategory(searchCategory: apiInput)) { (result: Result<MealsModel, Error>) in
                switch result {
                case .success(let result):
                    self.mealList = result.meals
                    print(self.mealList)
                    self.tableView.reloadData()
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return mealList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(mealList[indexPath.row].strMeal)
        print("it's id: \(mealList[indexPath.row].idMeal)")
        let mealId = mealList[indexPath.row].idMeal
        let imageName = mealList[indexPath.row].strMealThumb
        let mealName = mealList[indexPath.row].strMeal
        self.navigationController?.pushViewController(DetailsViewController(mealId: mealId, url: imageName, meal: mealName), animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomDisplayTableViewCell.identifier, for: indexPath) as! CustomDisplayTableViewCell

        let images = mealList[indexPath.row].strMealThumb
        let mealTitle = mealList[indexPath.row].strMeal
        cell.configureCell(image: images, title: mealTitle)

        return cell
    }

}
